import os
import random
import sys
import time
from time import sleep
from kazoo.client import KazooClient
from paho.mqtt import client as mqtt_client

kazoClient = None
kazoWaitTime = 30

topic = "aperturescience/sensors"
min_value = 0
max_value = 500
range_value = max_value - min_value
port = 1883
mqttWaitTime = 5
mqttCancelation = False
mqttIp = None
mqttId = "N/A"
pahoClient = None

# Init
random.seed(time.time() * 1000)
is_reader = random.random() <= 0.4

# ======================================
#  General methods
# ======================================

def on_connect_handler(clt, userdata, flags, rc):
    if rc == 0:
        print("Successfully connected")
        sys.stdout.flush()
        mqttCancelation = False
    else:
        print("Connection error")
        sys.stdout.flush()
        mqttCancelation = True

def on_disconnect_handler(clt, userdata, rc):
    print("Connection unexpectedly closed")
    sys.stdout.flush()
    mqttCancelation = True

# ======================================
#  END OF General methods
# ======================================



# Run code
if is_reader:
    print("READER:      Starting node...")
    sys.stdout.flush()
    mqttId = f'reader-{random.randint(0, 2022)}'
else:
    print("GENERATOR:   Starting node...", flush=True)
    sys.stdout.flush()
    mqttId = f'generator-{random.randint(0, 2022)}'

# Decision done, go kazoooo ...
zooHosts = os.environ['ZOO_SERVERS']
print(f"Found theese Zookeeper nodes: {zooHosts}.")
sleep(kazoWaitTime)
kazoClient = KazooClient(hosts=zooHosts)
kazoClient.start()

# Your turn Mqtt
while True:
    sleep(mqttWaitTime)
    mqttCancelation = False
    
    brokers = kazoClient.get_children("/root/cluster1")
    if len(brokers) <= 0:
        print("No brokers here!")
        sys.stdout.flush()
        sys.exit(-1)
    broker = random.choice(brokers)
    mqttIp = kazoClient.get(f"/root/cluster1/{broker}")[0].decode("utf-8")
    print(f"Broker node with IP '{mqttIp}'")
    sys.stdout.flush()

    try:
        pahoClient = mqtt_client.Client(mqttId, reconnect_on_failure=False)
        pahoClient.on_connect = on_connect_handler
        pahoClient.on_disconnect = on_disconnect_handler
        pahoClient.connect(mqttIp, port)
    except Exception:
        pahoClient = None
    if pahoClient is None:
        continue

    if is_reader:
        pahoClient.subscribe(topic)
        pahoClient.on_message = lambda clt, userdata, message: print(f"{message.payload.decode('utf-8')}", flush=True)
        pahoClient.loop_forever()
    else:
        pahoClient.loop_start()
        while not mqttCancelation:
            pahoClient.publish(topic, f"{mqttId}:"
                                            f"{min_value + random.random() * range_value}")
            sleep(random.randint(5, 25))
        pahoClient.loop_stop()

# Cleanup
kazoClient.stop()