#!/bin/sh

set -e
/usr/sbin/mosquitto -c /mosquitto/config/mosquitto.conf &
echo "Mosquitto started, activating dispatcher ..."
python3 -u /opt/broker/main.py