import os
import sys
from time import sleep
from kazoo.client import KazooClient
import threading
import socket
from paho.mqtt import client

broadcast_address = "255.255.255.255"

kazoClient = None
kazoWaitTime = 10

pahoClient = None

forward_symbol = "<!FW>"
separate_symbol = "<!SP>"
dispatcher_port = 5000
mosquitto_port = 1883
cluster = "cluster1"
id = "dispatcher"

myIp = socket.gethostbyname(socket.gethostname())

# ======================================
#  General methods
# ======================================
def broadcast_message(topic, content):
    print(f"\tBroadcast: {topic}")
    sys.stdout.flush()
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.sendto(f"{topic}{separate_symbol}{content}".encode("utf-8"), (broadcast_address, dispatcher_port))
                
def on_message_handler(clt, userdata, message):
    topic = message.topic
    content = message.payload.decode("utf-8")
    print(f"Incoming message: '{topic}', content: '{content}'")
    sys.stdout.flush()
    if forward_symbol in content:
        print("\tGarbagio message received...")
        sys.stdout.flush()
        return
    else:
        broadcast_message(topic, content)

def on_connect_handler(clt, userdata, flags, rc):
    if rc != 0:
        print("Unable to connect")
        sys.stdout.flush()
        exit(-1)
    pahoClient.subscribe("#")
    pahoClient.on_message = on_message_handler

@staticmethod
def on_disconnect_handler(clt, userdata, rc):
    print("Broker disconnected")
    sys.stdout.flush()
    exit(-1)

def receive_messages():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(("", dispatcher_port))
    while True:
        m = s.recvfrom(1024)
        if m[1][0] == myIp:
            continue
        content = m[0].decode("utf-8")
        # check format of the message
        if separate_symbol not in content:
            continue
        parts = content.split(separate_symbol)
        message = f"{forward_symbol}{parts[1]}"
        topic = parts[0]
        pahoClient.publish(topic, message.encode("utf-8"))


# ======================================
#  END OF General methods
# ======================================

# Run code
print("Connecting to Zookeeper node")
sys.stdout.flush()
sleep(kazoWaitTime)
zooHosts = os.environ['ZOO_SERVERS']
kazoClient = KazooClient(hosts=zooHosts)
kazoClient.start()
print("Connected to Zookeeper node")
sys.stdout.flush()

# Connected, proceed to Mqtt init
kazoClient.create(f"/root/{cluster}/node", makepath=True, ephemeral=True, sequence=True, value=os.environ["NODE_IP"].encode("utf-8"))
print(f"Broker:  '{os.environ['NODE_IP']}' added into cluster")
sys.stdout.flush()

#Mqtt connect 
pahoClient = client.Client(id)
pahoClient.on_connect = on_connect_handler
pahoClient.on_disconnect = on_disconnect_handler
pahoClient.connect(os.environ["NODE_IP"], port=mosquitto_port)

# Finally dispatcher ....

# Thread to communicate with others
receiving_thread = threading.Thread(target=receive_messages,
                                    daemon=True)
receiving_thread.start()


# Last but not least, neverending stooooory 
pahoClient.loop_forever()
