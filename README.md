# KIV-DS-4 - Cluster of MQTT Brokers with Zookeeper realization

# Zadání

Realizujte architekturu distribuovaného message brokeru s využitím Apache Zookeeper a MQTT brokeru Mosquitto.
Cílem je realizovat cluster MQTT brokerů, který je schopen transparentně obsloužit velké množství MQTT klientů.

Technické detaily:
- Klient nezná IP adresy MQTT brokerů, pouze referenci/jméno clusteru přes níž je schopen obdržet seznam funkčních MQTT brokerů v clusteru
- Pokud MQTT broker vypadne, je klient schopen přejít na jiný funkční
- Je možné provozovat více clusterů najednou

Mělo by odpovídat návrhu v práci DS-3

# Architektura

![](./img/kiv-ds-3-graph.drawio.png)

# Dodatečné informacce

Architektura obsahuje 3 hlavní bloky, všechny budou jako provozovány jako Docker kontejnery:

- MQTT Broker
  - Bude použit Mosquitto
  - Řídící program bude napsán v Pythonu
    - Provede registraci Brokeru v Zookeeperu
    - Drží si seznam ostatních brokerů
    - Syncronizuje zaslané zprávy mezi brokery

- MQTT Client
  - Bude použit program v Pythonu (Pravděpodobně s knihovnou pro MQTT komunikaci)
  - Zeptá se Zookeperu na dostupné Brokery
  - Vybere si z dostupných Brokerů a naváže spojení
  - V případě výpadku spojení se celá sekvence hledání Brokera opakuje

- Zookeeper
  - Apache Zookeeper
  - Cluster 3 instancí
  - Drží si informace o aktivních MQTT brokerech

# Jak to zapnout

Řešení vyžaduje Docker a Vagrant. Počet nodes v systému se dá nastavit parametry ZOOKEEPERS_COUNT, MOSQUITTOS_COUNT, CLIENTS_COUNT ve Vagrantfile. Defaultně je zde 5 klientů, 3 zookeepery a 3 mosquitto brokery. 

Ve složce řešení (./nodes) stačí zavolat příkaz ```vagrant up``` a provede se nasazení instancí ve vlastní síti v dockeru.

Vypnutí je analogické, příkaz ```vagrant destroy -f``` provede vypnutí a smazání celého řešení z prostředí docker.

## Jak to otestovat

Instance jsou nasazeny v prostředí docker, všechny výpisy jsou vidět v GUI Docker Desktop v sekci Logs (Logy kontejneru).  
Příkazem v konzoli, ```docker ps``` lze zjistit zdraví kontejnerů, v případě že je vše v pořádku, vedle času běhu je uvedelo healthy.
Mosquitto node obsahuje dispatcher, napsaný v Pythonu. Dispatcher svůj node registruje u Zookeperu, v rámci svého node má právě i Mosquitto. Dispatcher zároveň hlídá zprávy ostatních Mosquitto instancí v rámci clusteru, přeposlané zprávy jsou zobrazeny, ale již ne znovu přeposílány (Vznikla by bouře). Zpráva od klienta je přeposlána ostatním, s příznakem že se jedná o přeposlání.  
Klient, napsaný v pythonu. Dostane sezna Zookeeperů v systému, zvolí si 1 a požádá jej o seznam Mosquitto instancí. Náhodně vybere i Mosquitto instanci. Klient se rozhodne zda chce být posluchačem, či vydavatelem. Vydavatel generuje informace a rozesílá je do systému. Posluchač je přijímá a vypisuje na konzoli. Pokud je zpráva přijatá z jiného Mosquitta než u kterého jsme připojeni, má prefix ```<!FW>```.

# Závěr

Apliakci jsem navrhl dle zadání a s důrazem na DS-3 úkol. Dalo to trochu zabrat, ale vypadá to že je plně funkční.

